import unittest
from BinarySearchTree import BST

class BSTTestCase(unittest.TestCase):
	def testBinarySearch(self):
		bst = BST()

		bst.addNode(10, "Value 1")
		self.assertEqual(bst.size(),1)

		bst.addNode(5, "Value 2")
		self.assertEqual(bst.size(),2)

		bst.addNode(30, "Value 2")
		self.assertEqual(bst.size(),3)

		self.assertListEqual(bst.inOrderWalk(), [5, 10, 30])
		self.assertListEqual(bst.preOrderWalk(), [10, 5, 30])
		self.assertListEqual(bst.postOrderWalk(), [5, 30, 10])

		self.assertListEqual(bst.findSmallestKey(), 5)
		self.assertListEqual(bst.findLargestKey(), [30])

		self.assertEqual(bst.searchForNode(10), [1])

if __name__  == '__main__':
	unittest.main()