'''
	Array Implementation:
	root at position i.children at 2i, 2i+1. i starts at 1
	if empty, null
'''
'''
	Linked List Implementation:
	PointerL | Key | Value | PointerR
'''

class BST:
	def __init__(self):
		self._size = 0
		self._root = None #type bstnode

	class _BSTNode:
		def __init__(self, key, value):
			self.key = key
			self.value = value
			self.left = None
			self.right = None

	def addNode(self, key, value):
		z = self._BSTNode(key, value)
		y =None
		x = self._root
		while (x != None):
			y = x
			if (key < x.key):
				x = x.left
			else:
				x = x.right
		if (y == None):
			self._root = z
		elif (z.key < y.key):
			y.left = z
		else:
			y.right = z
		self._size += 1

	def isEmpty(self):
		return self._size == 0

	def size(self):
		return self._size

	def inOrderWalk(self):
		nodes = []
		self._inOrderWalk(self._root, nodes)
		return nodes

	def _inOrderWalk(self, subtree, nodes):
		if(subtree):
			self._inOrderWalk(subtree.left, nodes)
			nodes.append(subtree.key)
			self._inOrderWalk(subtree.right, nodes)

	def preOrderWalk(self):
		nodes = []
		self._preOrderWalk(self._root, nodes)
		return nodes

	def _preOrderWalk(self, subtree, nodes):
		if(subtree):
			nodes.append(subtree.key)
			self._preOrderWalk(subtree.left, nodes)
			self._preOrderWalk(subtree.right, nodes)

	def postOrderWalk(self):
		nodes = []
		self._postOrderWalk(self._root, nodes)
		return nodes

	def _postOrderWalk(self, subtree, nodes):
		if(subtree):
			self._postOrderWalk(subtree.left, nodes)
			self._postOrderWalk(subtree.right, nodes)
			nodes.append(subtree.key)

	def searchForNode(self, key):
		found = []
		self._searchForNode(self._root, key, found)
		return found

	def _searchForNode(self, subtree, key, found):
		if(subtree):
			if(key == subtree.key):
				found.append(1)
			elif(key < subtree.key):
				self._searchForNode(subtree.left, key, found)
			elif(key > subtree.key):
				self._searchForNode(subtree.right, key, found)

	def findSmallestKey(self):
		nodes = []
		self._findSmallestKey(self._root, nodes)
		return nodes

	def _findSmallestKey(self, subtree, nodes):
		if(subtree):
			if(subtree.left == None):
				nodes.append(subtree.key)
			self._findSmallestKey(subtree.left, nodes)

	def findLargestKey(self):
		nodes = []
		self._findLargestKey(self._root, nodes)
		return nodes

	def _findLargestKey(self, subtree, nodes):
		if(subtree):
			if(subtree.right == None):
				nodes.append(subtree.key)
			self._findLargestKey(subtree.right, nodes)
